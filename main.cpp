#include <iostream>
#include <libconfig.h++>

#include <unistd.h>
#include <list>

using namespace std;

std::ostream &output = std::cout;

class ICodePiece
{
public:
    virtual void print() = 0;
};

class SimpleText : public ICodePiece
{
private:
    string text;
public:
    explicit SimpleText(string text) : text(text) {}
    void print()
    {
        output << text << endl;
    }
};

class Include : public ICodePiece
{
private:
    string include_name;
    string struct_name;
public:
    explicit Include(string include, string structure) : include_name(include), struct_name(structure)
    {}
    void print()
    {
        output <<
                "#include \"" << include_name << '\"' << endl <<
                "int validate_config(struct config_class_context *cfg, struct " << struct_name << " *res)" << endl <<
                "{" << endl <<
                "if (cfg == NULL) {" << endl <<
                "  fprintf(stderr, \"NULL configuration ptr\\n\");" << endl <<
                "}" << endl;
    }
};

class LoadIntParameter : public ICodePiece
{
private:
    string variable_name;
public:
    explicit LoadIntParameter(string name) : variable_name(name) {}
    void print()
    {
        output
                << "int " << variable_name << ';' << endl
                << variable_name << " = cfg.getInt(cfg, \"" << variable_name << "\");" << endl;
    }
};

class IntParameterMin : public ICodePiece
{
private:
    string variable_name;
    int min_value;
public:
    explicit IntParameterMin(string name, int min_value) : variable_name(name), min_value(min_value) {}
    void print()
    {
        output <<
                "if (" << variable_name << " < " << min_value << ") {" << endl <<
                "  fprintf(stderr, \"" << variable_name << " must be at least " << min_value << "\");" << endl <<
                "}" << endl;
    }
};

class LoadLongParameter : public ICodePiece
{
private:
    string variable_name;
public:
    explicit LoadLongParameter(string name) : variable_name(name) {}
    void print()
    {
        output
                << "long " << variable_name << ';' << endl
                << variable_name << " = cfg.getLong(cfg, \"" << variable_name << "\");" << endl;
    }
};

class LoadStringParameter : public ICodePiece
{
private:
    string variable_name;
public:
    explicit LoadStringParameter(string name) : variable_name(name) {}
    void print()
    {
        output
                << "char *" << variable_name << ';' << endl
                << variable_name << " = cfg.getString(cfg, \"" << variable_name << "\");" << endl;
    }
};

std::list<ICodePiece *> code;

int main(int argc, char **argv) {
    string cfg_filename;
    int c;

    while ((c = getopt (argc, argv, "hc:")) != -1) {
        switch (c) {
            case 'c':
                cfg_filename = optarg;
                break;
            case 'h':
                cerr << "-c <filename.cfg>: uses config file" << endl;
                break;
            case '?':
                if (optopt == 'c') {
                    cerr << "Must provide a file name after -c" << endl;
                }
                break;
            default:
                exit(1);
        }
    }

    if (cfg_filename.empty())
    {
        cerr << "Must provide a file name" << endl;
        exit(1);
    }

    libconfig::Config conf;

    try {
        conf.readFile(cfg_filename.c_str());
    }
    catch(const libconfig::FileIOException &e) {
        cerr << "I/O error while reading file." << endl;
        return 1;
    }
    catch(const libconfig::ParseException &e)
    {
        std::cerr << "Parse error at " << e.getFile() << ":" << e.getLine()
                << " - " << e.getError() << endl;
        return 1;
    }

    string include_name, struct_name;
    conf.lookupValue("core.include", include_name);
    conf.lookupValue("core.struct", struct_name);
    code.push_back(new Include(include_name, struct_name));

    auto &p = conf.lookup("parameters");

    int l = p.getLength();
    for (int i = 0; i < l; i++) {
        cout << p[i].getName() << endl;
        string type;
        p[i].lookupValue("type", type);
        if (type == "int") {
            code.push_back(new LoadIntParameter(p[i].getName()));
            if (p[i].exists("min")) {
                code.push_back(new IntParameterMin(p[i].getName(), p[i]["min"]));
            }
        } else if (type == "long") {
            code.push_back(new LoadLongParameter(p[i].getName()));
        } else if (type == "string") {
            code.push_back(new LoadStringParameter(p[i].getName()));
        }
    }

    code.push_back(new SimpleText("return 0;\n}"));

    for(auto it = code.begin(); it != code.end(); it++)
    {
        (*it)->print();
    }

    return 0;
}